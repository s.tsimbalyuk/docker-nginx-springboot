package dev.tsvinc.apptwo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Cntrlr {
    private static final Logger log = LoggerFactory.getLogger(Cntrlr.class);

    @Value("${spring.application.name}")
    private String appName;

    @GetMapping(value = "/ok", consumes = "*/*", produces = "*/*")
    public ResponseEntity<String> ok() {
        log.info("OK in app: {}", appName);
        return ResponseEntity.ok("OK from " + appName);
    }
}
