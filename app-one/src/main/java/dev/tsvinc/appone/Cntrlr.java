package dev.tsvinc.appone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@RestController
public class Cntrlr {
    private static final Logger log = LoggerFactory.getLogger(Cntrlr.class);

    @Value("${spring.application.name}")
    private String appName;

    private final HttpClient httpClient;

    public Cntrlr(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    @GetMapping(value = "/", consumes = "*/*", produces = "*/*")
    public ResponseEntity<String> root() {
        log.info("ROOT in app: {}", appName);
        return ResponseEntity.ok("OK from " + appName);
    }

    @GetMapping(value = "/ok", consumes = "*/*", produces = "*/*")
    public ResponseEntity<String> ok() {
        log.info("OK in app: {}", appName);
        return ResponseEntity.ok("OK from " + appName);
    }

    @PostMapping(value = "/check-url")
    public ResponseEntity<String> checkUrl(@RequestBody UrlHolder url) {
        log.info("check-url in app: {}, url: {}", appName, url.url);
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(url.url))
                .setHeader("User-Agent", appName + " client")
                .build();
        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            log.info(
                    "[{}] scheduled task. response status code: {}, body: {}",
                    appName,
                    response.statusCode(),
                    response.body());
        } catch (IOException | InterruptedException e) {
            log.error("[{}] error: {}", appName, e.getMessage());
            Thread.currentThread().interrupt();
        }
        return ResponseEntity.ok("OK from " + appName);
    }
}
