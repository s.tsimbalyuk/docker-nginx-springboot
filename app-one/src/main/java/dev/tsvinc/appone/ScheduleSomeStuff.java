package dev.tsvinc.appone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

@Component
public class ScheduleSomeStuff {
    private static final Logger log = LoggerFactory.getLogger(ScheduleSomeStuff.class);

    private final TaskScheduler taskScheduler;
    private final HttpClient httpClient;

    @Value("${spring.application.name}")
    private String appName;

    @Value("${config.repeat-interval}")
    private Integer repeatInterval;

    @Value("${config.app-two.url}")
    private String appTwoUrl;

    public ScheduleSomeStuff(TaskScheduler taskScheduler, HttpClient httpClient) {
        this.taskScheduler = taskScheduler;
        this.httpClient = httpClient;
    }

    @ConditionalOnProperty(value = "config.schedule.enabled", havingValue = "true")
    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
        log.info("app {} started. Scheduling tasks...", appName);
        taskScheduler.scheduleAtFixedRate(
                () -> {
                    HttpRequest request = HttpRequest.newBuilder()
                            .GET()
                            .uri(URI.create(appTwoUrl))
                            .setHeader("User-Agent", appName + " client")
                            .build();
                    try {
                        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
                        log.info(
                                "[{}] scheduled task. response status code: {}, body: {}",
                                appName,
                                response.statusCode(),
                                response.body());
                    } catch (IOException | InterruptedException e) {
                        log.error("[{}] error: {}", appName, e.getMessage());
                        Thread.currentThread().interrupt();
                    }
                },
                Duration.of(repeatInterval, ChronoUnit.SECONDS));
    }
}
